import React,{Component} from 'react';
import { 
  TouchableOpacity,
  StyleSheet,
  Viwe,
  Text
} from 'react-native';

export default class ButtonAdd extends Component{
  
  constructor(props) {
    super(props);
  }

  render(){
    return (
      <TouchableOpacity style={styles.button} onPress={ this.props.onPress }>
        <Text style = {styles.buttonText}>{this.props.children}</Text>
        
      </TouchableOpacity>
    )
  }

}

/*
const ButtonMade = ({children}) =>{
    return(
        <TouchableOpacity style={styles.button} 
            onPress={
                console.log('button pressed!')}
        >
            <Text style = {styles.buttonText}>{children}</Text>
        </TouchableOpacity>        
    );   
}
*/
const styles=StyleSheet.create(
    {
        button:{
            height: 30,
            margin: 10,
            padding: 20,
            //paddingLeft: 20,
            //paddingRight: 20,
            backgroundColor: '#a42d00',
            borderRadius: 20,
            alignItems: 'center',
            justifyContent: 'center',
        },
        buttonText:{
            color: '#ffffff',
            fontSize: 20,
            fontWeight: 'bold',
        },
    }
);