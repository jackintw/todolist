/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import {
  Navigator
} from 'react-native-deprecated-custom-components'
import observableContentStore from './app/mobx/contentStore'
import App from './app/App'



export default class Todolist extends Component {
  
  constructor(props){
    super(props);
  }

  renderScene(route, navigator){
    return <route.component {...route.passProps} navigator={navigator} />
  }

  configureScene (route, routeStack) {
    if (route.type === 'Modal') {
      return Navigator.SceneConfigs.FloatFromBottom
    }
    return Navigator.SceneConfigs.PushFromRight
  }

  render () {
    return (
      <Navigator
        configureScene={this.configureScene.bind(this)}
        renderScene={this.renderScene.bind(this)}
        initialRoute={{
          component: App,
          passProps: {store: observableContentStore}
        }} 
      />
  )}
}


AppRegistry.registerComponent('Todolist', () => Todolist);
