import {observable} from 'mobx'


let index = 0;

class ObservableContentStore{

  @observable list = []

  addContent(title, item){
    this.list.push({
      title: title,
      content: item,
      index
    })
    index++
  }
  
  removeContent(item){
    this.list = this.list.filter((l) => {
      return l.index !==item.index;
    })
  }


}