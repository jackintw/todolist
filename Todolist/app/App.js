import React, {Component} from 'react'
import 
{ View,
  Text,
  TextInput,
  TouchableHightlight,
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView
} from 'react-native'
import { observer} from 'mobx-react/native'
import ButtonAdd from '../components/ButtonAdd'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'



@observer
export default class Todolist extends Component {

  constructor(props) {
    super(props);
    this.state = {
      titleEnter: '',
      contentEnter: '',
      inputShow: false, 
      scrollEnabled: false
    }
  }

  toggleInput() {
    this.setState(
      { input: !this.state.input}
    )
  }

  addItemToList() {
    //save title and content user entered to mobx store
    this.props.store.addContent(this.state.titleEnter, this.state.contentEnter)
    //after saving, reset
    this.setState({
      text: '',
    })
  }

  removeItemFromList(item) {
    this.props.store.removeContent(item)
  }

  updateTitle(title) {
    this.setState({titleEnter:title})
  }

  updateContent(content) {
    this.setState({contentEnter: content})
  }

  scrollEnabled() {
    this.setState({scrollEnabled: true})
  }

  scrollUnabled() {
    this.setState({scrollEnabled: false})
  }

  render() {
    const {inputShow} = this.state
    const {list} = this.props.store

    return(
      <KeyboardAwareScrollView
      style={{ backgroundColor: '#ffddaa' ,}}
      resetScrollToCoords={{ x: 0, y: 0 }}
      contentContainerStyle={{flex:1}}
      scrollEnabled={this.state.scrollEnabled}
      >
      <View style={styles.mainView}> 
        <View style={styles.listView}>
        </View>

        <ButtonAdd 
          style={styles.buttonEnterView}
          onPress={ () => {
            this.setState({ inputShow: !inputShow })
        }}>
          { this.state.inputShow ? "Send":"+Add New Notes"}
        </ButtonAdd>  

      
          { this.state.inputShow &&
            <View style={styles.enterView}>
              <TextInput
                style={styles.titleInputView}
                autoFocus={false} placeholder={"請輸入標題"} 
                textAlign={'left'} fontSize={20} 
                onFocus={this.scrollEnabled.bind(this)} 
                onBlur={this.scrollUnabled.bind(this)} 
              />      
              <TextInput 
                style={styles.contentInputVIew}
                numberOfLines={5}
                autoFocus={false} multiline={true} 
                placeholder={"請輸入內容"} fontSize={20}                                    
                onFocus={this.scrollEnabled.bind(this)} 
                onBlur={this.scrollUnabled.bind(this)}
              /> 
            </View>
          }
         
       
      </View>
      </KeyboardAwareScrollView>

        

    )
  }


}

const styles = StyleSheet.create({
  mainView: {
    flexDirection: 'column',
    flex: 1,
    backgroundColor: '#ffddaa',
    justifyContent: 'space-between'
  },

  listView: {
    flexDirection: 'column',
    flex:3
  },

  enterView: {
    flexDirection: 'column',
    flex: 3,
    margin:15
  },

  buttonEnterView: {
    flexDirection: 'row'
  },

  contentEnterView: {
    flexDirection: 'row',
  },

  titleInputView: {
    flex:1,
    backgroundColor: '#ffbb66',
    padding: 10,
    color:'#bb5500'
  },

  contentInputVIew: {
    textAlignVertical:'top',
    flex:4,
    backgroundColor: '#ffbb66',
    padding: 10,
    marginBottom:10,
    color: '#bb5500',

  },




  })
